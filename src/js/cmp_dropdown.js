class Dropdown {
  /**
  * Elements with class dropdown in the DOM.
  * @constructor
  * @param {string} actionDropdown Buttons list class of the Dropdown
  * @param {string} classShow Class for show the content
  * @param {string} classHide Class for hide the content
  */
  constructor(actionDropdown, classShow, classHide) {
    this.actionDropdown = Array.from(
      document.querySelectorAll(actionDropdown));
    this.classShow = classShow;
    this.classHide = classHide;
    this.init();
  }

  /**
  * Principal function in the Class
  */
  init() {
    this.showFormLogInOnLoad();
    this.showOrHideContent();
  }

  /**
  * Show content of form login when loading the page
  * @param {string} btnDropdownLogin Dropdown of Login form
  * @param {string} parentDropdownLogin Parent Dropdown of Login form
  */
  showFormLogInOnLoad() {
    window.addEventListener("load", () => {
      var btnDropdownLogin = this.actionDropdown[0];
      var parentDropdownLogin = btnDropdownLogin.closest(".cmp-dropdown");

      this.addClassToShow(parentDropdownLogin, btnDropdownLogin);
    })
  }

  /**
  * Show or Hide content of Current dropdown
  * @param {string} parent_btn Parent Current dropdown
  */
  showOrHideContent() {
    this.actionDropdown.forEach(currentBtn => {
      currentBtn.addEventListener("click", event => {

        this.hideContentSiblings(currentBtn);

        var parent_btn = event.target.parentElement;

        if (parent_btn.classList.contains(this.classHide)) {
          this.addClassToShow(parent_btn, currentBtn);
        } else {
          this.AddClassToHide(parent_btn, currentBtn);
        }

      });
    });
  }

  /**
  * Hide content of sibling when click in the dropdown
  * @param {string} parentBtns List parents of component dropdown
  */
  hideContentSiblings(currentBtn) {
    this.actionDropdown.forEach(listBtns => {

      var parentBtns = listBtns.closest(".cmp-dropdown");

      if (currentBtn.classList != listBtns.classList) {
        this.AddClassToHide(parentBtns, listBtns);
      }

    });
  }

  /**
  * Add the respective class for show dropdown
  */
  addClassToShow(parentBtn, btnDropdown) {
    btnDropdown.classList.add("is-dropdown-active");
    parentBtn.classList.add(this.classShow);
    parentBtn.classList.remove(this.classHide);
  }

  /**
  * Add the respective class for hide dropdown
  */
  AddClassToHide(parentBtn, btnDropdown) {
    parentBtn.classList.remove(this.classShow);
    btnDropdown.classList.remove("is-dropdown-active")
    parentBtn.classList.add(this.classHide);
  }
}

// Instance of dropdown in the component AUTH.
new Dropdown(".js-dropdown-action", "is-dropdown-show", "is-dropdown-hide");