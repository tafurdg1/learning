# Learning
&nbsp;

Educational project that allow of students and teachers have comunication through a Blog.

&nbsp;

---
&nbsp;
## License

* MIT License, **URL: learning/LICENSE**

&nbsp;

--- 
&nbsp;
## Structure files

    - learning (/)

      - assets (files with font or imgs)

      - dist (Main file with compile project)

      - src (Local dev environment)

        - JS (functions of system)

        - SCSS (Styles of system)

        - TEMPLATE (Sctructures of system)
&nbsp;

---
&nbsp;
## Software required

* Code kit
* Fork
* VSC Visual Studio Code or another environment